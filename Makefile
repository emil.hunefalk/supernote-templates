
init:
	git config core.hooksPath .githooks
	npm install

check:
	.scripts/sanity

mogrify-jpg:
	mogrify -format png *.jpg

pngcrush:
	pngcrush -d templates *.png

TEMPLATEJSONS = $(shell find templates -name '*.json')

public: site/index.html $(TEMPLATEJSONS)
	rm -rf public
	mkdir -p public
	jq -s 'add' templates/*.json > public/index.json
	cp templates/*.png public
	cp site/index.html public/index.html

